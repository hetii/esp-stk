# ESPSTK #

This is the port of stk500v2 programmer for esp8266.
Probably still place for a lot of new features. 

### What is this repository for? ###

* By this hardware you should be able to reprogram any AVR devices that can be handled by STK500v2 programmer.
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Follow [esp-open-rtos](https://github.com/SuperHouse/esp-open-rtos) project and prepare just esp-open-sdk in your desktop.
* Clone this repository and jump into `firmware`
* type `make` and wait till esp-open-rtos with libs be prepared.
* Update `esp-open-rtos/include/ssid_config.h` and put your own credentials to access point.
* type again `make` for final compilation.
* Test if you can connect to your target device eg: `avrdude -v -p t45 -c stk500v2 -P net:192.168.0.35:24`

### Network services ###
* port 23 - telnet port (used by uart bridge)
* port 24 - avrdude port (used stk500v2 programmer)

### AVRDUDE SPI SCK mapping ###

avrdude is able to control SPI clock, as I meet different variation regarding this subject I decide to put own mapping for this "-B" argument:

```
---------------------------------
|avrdude -B value| ISP Frequency|
---------------------------------
|        1       |     10MHz    |
|        3       |     8MHz     |
|        9       |     4MHz     |
|        18      |     3MHz     |
|        19      |     2.5MHz   |
|        23      |     2MHz     |
|        26      |     1.5MHz   |
|        29      |     1MHz     |
|        33      |     500KHz   |
|        36      |     250KHz   |
|        39      |     125KHz   |
|        42      |     62KHz    |
|        46      |     31KHz    |
---------------------------------
```

Default clock (No -B argument) is set to 250KHz.

If you plan to change that default value please edit stk500.c
in line `11, 0, 0xaa, 0, 0, 0, 0, 0,` of stkParam_t structure (first digit).

This first digit is a value of `switch(stk500Delay)` statement in `ispAttachToDevice()` method. 

### TODOs ###

* Add configuration for uart bridge.
* Add support for other uC.
* Add support for UrJTAG project.

### Prototype board: ###

![espstk_bottom.jpg](https://bitbucket.org/repo/KBMnnM/images/961741237-espstk_bottom.jpg)
![espstk_top.jpg](https://bitbucket.org/repo/KBMnnM/images/1183696165-espstk_top.jpg)

### Result :) ###
```
 clear && time avrdude -v  -p t45 -c stk500v2 -P net:192.168.0.35:24 -U flash:w:avrfid.hex:i

avrdude: Version 6.1, compiled on Sep  8 2015 at 09:40:37
         Copyright (c) 2000-2005 Brian Dean, http://www.bdmicro.com/
         Copyright (c) 2007-2014 Joerg Wunsch

         System wide configuration file is "/etc/avrdude.conf"
         User configuration file is "/home/hetii/.avrduderc"
         User configuration file does not exist or is not a regular file, skipping

         Using Port                    : net:192.168.0.35:24
         Using Programmer              : stk500v2
         AVR Part                      : ATtiny45
         Chip Erase delay              : 4500 us
         PAGEL                         : P00
         BS2                           : P00
         RESET disposition             : possible i/o
         RETRY pulse                   : SCK
         serial program mode           : yes
         parallel program mode         : yes
         Timeout                       : 200
         StabDelay                     : 100
         CmdexeDelay                   : 25
         SyncLoops                     : 32
         ByteDelay                     : 0
         PollIndex                     : 3
         PollValue                     : 0x53
         Memory Detail                 :

                                  Block Poll               Page                       Polled
           Memory Type Mode Delay Size  Indx Paged  Size   Size #Pages MinW  MaxW   ReadBack
           ----------- ---- ----- ----- ---- ------ ------ ---- ------ ----- ----- ---------
           eeprom        65     6     4    0 no        256    4      0  4000  4500 0xff 0xff
           flash         65     6    32    0 yes      4096   64     64  4500  4500 0xff 0xff
           signature      0     0     0    0 no          3    0      0     0     0 0x00 0x00
           lock           0     0     0    0 no          1    0      0  9000  9000 0ox00 0x00
           lfuse          0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           hfuse          0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           efuse          0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           calibration    0     0     0    0 no          2    0      0     0     0 0x00 0x00

         Programmer Type : STK500V2
         Description     : Atmel STK500 Version 2.x firmware
         Programmer Model: STK500
         Hardware Version: 1
         Firmware Version Master : 2.04
         Topcard         : STK501
         Vtarget         : 5.0 V
         SCK period      : 32.0 us
         Varef           : 0.0 V
         Oscillator      : 28.577 kHz

avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.03s

avrdude: Device signature = 0x1e9206
avrdude: safemode: lfuse reads as E2
avrdude: safemode: hfuse reads as DF
avrdude: safemode: efuse reads as FF
avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
         To disable this feature, specify the -D option.
avrdude: erasing chip
avrdude: reading input file "avrfid.hex"
avrdude: writing flash (596 bytes):

Writing | ################################################## | 100% 0.40s

avrdude: 596 bytes of flash written
avrdude: verifying flash memory against avrfid.hex:
avrdude: load data flash data from input file avrfid.hex:
avrdude: input file avrfid.hex contains 596 bytes
avrdude: reading on-chip flash data:

Reading | ################################################## | 100% 0.31s

avrdude: verifying ...
avrdude: 596 bytes of flash verified

avrdude: safemode: lfuse reads as E2
avrdude: safemode: hfuse reads as DF
avrdude: safemode: efuse reads as FF
avrdude: safemode: Fuses OK (E:FF, H:DF, L:E2)

avrdude done.  Thank you.


real	0m1.853s
user	0m0.036s
sys	0m0.008s
```